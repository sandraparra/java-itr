package com.codeoftheweb.salvo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/api")
public class SalvoController {

    @Autowired
    GameRepository gameRepo;
    @Autowired
    GamePlayerRepository gamePlayerRepo;

    @RequestMapping("/games")
    public Map<String, Object> makeLoggedPlayer(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null){
            dto.put("player","Guest");
        } else {
            dto.put("player",loggedPlayerDTO(authenticatedPlayer));
        }
        dto.put("games",getGames());
        return  dto;
    }
/*    public List<Object> dameTodosLosjuegos() {
        return gameRepo.findAll().stream().map(game -> gameDTO(game)).collect(Collectors.toList());
    }*/

    private Map<String, Object> loggedPlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("name", player.getEmail());

        return dto;
    }
    private Player getAuthentication(Authentication authentication) {
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        } else {
            return (playerRepo.findByEmail(authentication.getName()));
        }
    }


    public List<Object> getGames() {
        return gameRepo
                .findAll()
                .stream()
                .map(game -> gameDTO(game))
                .collect(Collectors.toList());
    }

    private Map<String, Object> gameDTO(Game game) {
            Map<String, Object> dto = new LinkedHashMap<String, Object>();

            dto.put("id", game.getId());
            dto.put("created", game.getDate().getTime());
            dto.put("gamePlayers", gamePlayersLista(game.getGamePlayers()));
            dto.put("scores", game.getScores().stream().map(score -> scoresDTO(score)));
            return dto;
    }

    private List<Object> gamePlayersLista(Set<GamePlayer> gamePlayers) {
             return gamePlayers
                    .stream()
                    .map(gamePlayer -> gamePlayersDTO(gamePlayer))
                    .collect(Collectors.toList());
        }



    @RequestMapping("/game_view/{id}")
    public Map<String, Object> getGameView (@PathVariable long id) {
        return gameViewDTO(gamePlayerRepo.findById(id).get());
    }

    private Map<String, Object> gameViewDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getDate().getTime());
        dto.put("gamePlayers", gamePlayersLista(gamePlayer.getGame().getGamePlayers()));
        dto.put("ships", gamePlayer.getShips());
        dto.put("salvoes", gamePlayer.getGame().getGamePlayers().stream().flatMap(gamePlayer1 -> gamePlayer1.getSalvoes().stream().map(salvo -> salvosDTO (salvo))));
        dto.put("score", gamePlayer.getPlayer().getScores().stream().map(score -> scoresDTO(score)));
        return dto;
    }


    private Map<String, Object> salvosDTO(Salvo salvo) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("locations", salvo.getLocations());
        return dto;
    }

    private Map<String, Object> scoresDTO(Score score) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("playerID", score.getPlayer().getId());
        dto.put("score", score.getScore());
        dto.put("finishDate", score.getFinishDate().getTime());
        return dto;
    }
    private Map<String, Object> playersDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("email", player.getEmail());
        dto.put("score", leadersBoardDTO(player));
        return dto;
    }
    private Map<String, Object> gamePlayersDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", gamePlayer.getId());
        dto.put("player", playersDTO(gamePlayer.getPlayer()));

        return dto;
    }
    @Autowired
    PlayerRepository playerRepo;
    @RequestMapping("/leaderBoard")
    public List<Object> getAllScores() {
        return playerRepo
                .findAll()
                .stream()
                .map(player -> playersDTO(player))
                .collect(Collectors.toList());
    }

    private Map<String, Object> leadersBoardDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("name", player.getEmail());
        dto.put("total", player.getScoresTotal(player));
        dto.put("won", player.getWins(player.getScores()));
        dto.put("lost", player.getLost(player.getScores()));
        dto.put("tied", player.getTied(player.getScores()));
        return dto;
    }



    //LOGIN LOGOUT
  @Autowired
  private PasswordEncoder passwordEncoder;

  @RequestMapping(path = "/players", method = RequestMethod.POST)
  public ResponseEntity<Object> register(
      @RequestParam String username, @RequestParam String password) {

    if (username.isEmpty() || password.isEmpty()) {
      return new ResponseEntity<>("No name given", HttpStatus.FORBIDDEN);
    }

    if (playerRepo.findByEmail(username) !=  null) {
      return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
    }

      playerRepo.save(new Player( username, passwordEncoder.encode(password)));
    return new ResponseEntity<>("Player added",HttpStatus.CREATED);
  }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame(Authentication authentication) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null){
            return new ResponseEntity<>(makeMap("error","Your are not registered"), HttpStatus.FORBIDDEN);
        } else {
            Date date = Date.from(java.time.ZonedDateTime.now().toInstant());
            Game auxGame = new Game(date);
            gameRepo.save(auxGame);
            GamePlayer auxGameP = new GamePlayer(auxGame,authenticatedPlayer);
            gamePlayerRepo.save(auxGameP);
            return new ResponseEntity<>(makeMap("gpid", auxGameP.getId()), HttpStatus.CREATED);
        }
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }
    @RequestMapping(path = "/game/{id}/players", method = RequestMethod.POST)
    public ResponseEntity<Object> joinGame(Authentication authentication, @PathVariable long id) {

        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);

        Game gameActual = gameRepo.findById(id).get();

        if (authenticatedPlayer == null) {
            return new ResponseEntity<>("No such player", HttpStatus.UNAUTHORIZED);
        }

        if (gameActual == null) {
            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
        }
else {
    GamePlayer newGamePlayer= new GamePlayer(gameActual, authenticatedPlayer);
    gamePlayerRepo.save(newGamePlayer);
            return new ResponseEntity<>("Game join succesfully",HttpStatus.CREATED);
        }
    }
}
