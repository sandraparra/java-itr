package com.codeoftheweb.salvo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;


@Entity
public class Game {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
        @GenericGenerator(name = "native", strategy = "native")
        private long id;
        private Date date = new Date();

//UN JUEGO PARA MUCHOS GAMEPLAYER
    @OneToMany(mappedBy="game", fetch=FetchType.EAGER)
    private
    Set<GamePlayer> gamePlayers;

//UN JUEGO PARA MUCHOS SCORES
    @OneToMany(mappedBy="game", fetch=FetchType.EAGER)
    private
    Set<Score> scores;

//CONSTRUCTOR VACIO
    public Game() {
    }

//CONSTRUCTOR CON SOBRECARGA
    public Game(Date date) {
            this.date = date;
        }

//SET Y GET DE DATE
    public Date getDate() {
            return date;
        }
    public void setDate(Date date) {
            this.date = date;
        }

//GET DE ID
    public long getId() {
        return id;
    }

//SET Y GET DE LISTA DE GAMEPLAYER
    public Set<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }
    public void setGamePlayers(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

//AÑADIR UN JUEGO A LA LISTA GAMEPLAYERS
    public void addGamePlayer(GamePlayer gamePlayer) {
        gamePlayer.setGame(this);
        getGamePlayers().add(gamePlayer);
}


    public Set<Score> getScores() {
        return scores;
    }
    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public void addScore(Score score) {
        score.setGame(this);
        getScores().add(score);
    }
}






