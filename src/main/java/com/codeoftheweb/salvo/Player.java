package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import java.util.Set;


@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String email;
    private String password;

//UN PLAYER PARA MUCHOS GAMEPLAYE
    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private
    Set<GamePlayer> gamePlayers;

// UN PLAYER PARA MUCHOS SCORES
    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private
    Set<Score> scores;

//CONSTRUCTOR VACIO
    public Player() { }
//CONSTRUCTOR CON SOBRECARGA
    public Player(/*String fullName,*/ String email, String password) {
       // name = fullName;
        this.email = email;
        this.password = password;
    }
//SET GET PASSWORD
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    // SET GET USERNAME
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
//GET DE ID
    public long getId() {
        return id;
    }
//SET Y GET DE GAMEPLAYERS LISTA
    public Set<GamePlayer> getGamePlayers() {
        return gamePlayers;
    }
    public void setGamePlayers(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }
//AÑADIR UN  PLAYER A LA LISTA DE  GAMEPLAYERS
    public void addGamePlayer(GamePlayer gamePlayer) {
        gamePlayer.setPlayer(this);
        getGamePlayers().add(gamePlayer);
    }

    //SET Y GET DE GAMEPLAYERS LISTA

    public Set<Score> getScores() {
        return scores;
    }
    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    //AÑADIR UN  PLAYER A LA LISTA DE  GAMEPLAYERS
    public void addScore(Score score) {
        score.setPlayer(this);
        getScores().add(score);
    }





    public float getScoresTotal (Player player) {
        return getWins(player.getScores())*1
                + getLost(player.getScores())*0
                + getTied(player.getScores())*(float)0.5;
    }

    public float getWins (Set<Score> scores) {
        return scores
                .stream()
                .filter(score -> score.getScore() == 1)
                .count();
    }

    public float getLost (Set<Score> scores) {
        return scores
                .stream()
                .filter(score -> score.getScore() == 0)
                .count();
    }

    public float getTied (Set<Score> scores) {
        return scores
                .stream()
                .filter(score -> score.getScore() == 0.5)
                .count();
    }
}

