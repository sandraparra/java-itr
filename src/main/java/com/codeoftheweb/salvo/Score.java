package com.codeoftheweb.salvo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private float score;
    private Date finishDate = new Date();

    //MUCHOS SCORE PARA UNPLAYER
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

    //MUCHOS SCORE PARA UN GAME
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;

    //CONSTRUCTOR VACIO
    public Score() { }

    //CONSTRUCTOR CON SOBRECARGA
    public Score(Game game, Player player, float score, Date finishDate) {
        this.game = game;
        this.player = player;
        this.score = score;
        this.finishDate = finishDate;
    }

    //GET DE ID
    public long getId() {
        return id;
    }
    //SET Y GET DE PLAYER

    public void setPlayer (Player player){
        this.player = player;
    }
    @JsonIgnore
    public Player getPlayer (){
        return player;
    }
    //SET Y GET DE GAME

    public void setGame(Game game){
        this.game = game;
    }
    @JsonIgnore
    public Game getGame (){
        return game;
    }


    public float getScore() {
        return score;
    }
    public void setScore(float score) {
        this.score = score;
    }


    public Date getFinishDate() {
        return finishDate;
    }
    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }
}
