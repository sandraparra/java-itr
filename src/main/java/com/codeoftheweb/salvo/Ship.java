package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity

public class Ship {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
   @GenericGenerator(name = "native", strategy = "native")
   private long id;
   private String type;

//MUCHOS BARCOS PARA UN GAMEPLAYER
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gamePlayer_id")
    private GamePlayer gamePlayer;

    @ElementCollection
    @Column(name="locations")
    private List<String> locations = new ArrayList<String>();


   public Ship() { }

   public Ship(String type, GamePlayer gamePlayer,List<String> locations  ) {
            this.type = type;
            this.gamePlayer=gamePlayer;
            this.locations = locations;
            }

   public String getType() {
            return type;
        }
   public void setType(String type) {
            this.type = type;
        }

   public long getId() {
            return id;
        }

   public List<String> getLocations() {
        return locations;
    }
   public void setLocations(List<String> location) {
        this.locations = location;
    }

    @JsonIgnore
    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }
    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }
}
