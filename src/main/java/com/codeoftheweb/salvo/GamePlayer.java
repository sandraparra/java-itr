package com.codeoftheweb.salvo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
public class GamePlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
//MUCHOS GAMEPLAYER PARA UNPLAYER
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

//MUCHOS GAMEPLAYER PARA UN GAME
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;
//UN GAME PLAYER PARA MUCHOS SHIP
    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private
    Set<Ship> ships;
    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private
    Set<Salvo> salvoes;


//CONSTRUCTOR VACIO
    public GamePlayer() { }
//CONSTRUCTOR CON SOBRECARGA
    public GamePlayer(Game game, Player player) {
        this.game = game;
        this.player = player;
    }

//GET DE ID
    public long getId() {
        return id;
    }
//SET Y GET DE PLAYER
    public void setPlayer (Player player){
        this.player = player;
}
    @JsonIgnore
    public Player getPlayer (){
        return player;
    }
//SET Y GET DE GAME
    public void setGame(Game game){
        this.game = game;
    }
    @JsonIgnore
    public Game getGame (){
        return game;
    }


    public Set<Ship> getShips() {
        return ships;
    }
    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }
    public void addShip(Ship ship) {
        ship.setGamePlayer(this);
        getShips().add(ship);
    }


    public Set<Salvo> getSalvoes() {
        return salvoes;
    }
    public void setSalvoes(Set<Salvo> salvoes) {
        this.salvoes = salvoes;
    }

    public void addSalvo(Salvo salvo) {
        salvo.setGamePlayer(this);
        getSalvoes().add(salvo);
    }
}
